;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/functions
  (:use :common-lisp)
  (:export :add-ignored-arguments
           :curry
           :rcurry))

(in-package :clean-util/source/functions)

(defun curry (function &rest arguments)
  "CURRY returns a closure that applies FUNCTION to ARGUMENTS and the arguments
 passed to that closure."
  (lambda (&rest more-arguments)
    (multiple-value-call function
      (values-list arguments)
      (values-list more-arguments))))

(defun rcurry (function &rest arguments)
  "RCURRY returns a closure that applies FUNCTION to the arguments passed to
 that closure and ARGUMENTS."
  (lambda (&rest more-arguments)
    (multiple-value-call function
      (values-list more-arguments)
      (values-list arguments))))

(defun %n-gensyms (n)
  (loop :for _ :below n :collect (gensym)))

(defmacro add-ignored-arguments (function front passed back)
  "ADD-IGNORED-ARGUMENTS returns a closure that takes (+ FRONT PASSED BACK)
number of arguments, but only passes the middle PASSED number argument to
FUNCTION."
  (let ((front-args (%n-gensyms front))
        (passed-args (%n-gensyms passed))
        (back-args (%n-gensyms back)))
    `(lambda (,@front-args ,@passed-args ,@back-args)
       (declare (ignore ,@front-args ,@back-args))
       (funcall ,function ,@passed-args))))

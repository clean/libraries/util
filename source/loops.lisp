;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/loops
  (:use :common-lisp

        :clean-util/source/macros)
  (:export :bound-exceeded
           :bounded-dolist
           :until
           :while))

(in-package :clean-util/source/loops)

(defmacro while (predicate &body body)
  "Evaluate BODY as long as PREDICATE evaluates to true.

Parameters:

PREDICATE (FORM)
BODY (FORMS)

Description:

Alternately evaluate PREDICATE and BODY starting with PREDICATE as long as evaluating PREDICATE returns true. Return NIL."
  `(loop :while ,predicate :do (progn ,@body)))

(defmacro until (predicate &body body)
  "Evaluating BODY until PREDICATE evaluates to true.

Parameters:

PREDICATE (FORM)
BODY (FORMS)

Description:

Alternately evaluate PREDICATE and BODY starting with PREDICATE until evaluating PREDICATE returns true. Return NIL."
  `(while (not ,predicate) ,@body))

(defun %bound-exceeded-reporter (error stream)
  (format stream "BOUNDED-LIST hit bound."))

(define-condition bound-exceeded (error) ()
  (:report %bound-exceeded-reporter)
  (:documentation "Signaled when BOUNDED-DOLIST hits its bound."))

(defmacro bounded-dolist ((var list bound &optional result) &body body)
  "Evaluate BODY once for each element of LIST, but signal an error after BOUND iterations.

Parameters:

VAR (SYMBOL)
LIST (LIST)
MAX-ITERATIONS (INTEGER)
RESULT (FORM)
BODY (FORMS)

Description:

Evaluate BODY once for each element of LIST. The symbol VAR is bound to the current element within BODY for each iteration. Finally return RESULT. After BOUND iterations signals BOUND-EXCEEDED, if LIST has more than BOUND elements.

Errors:

BOUND-EXCEEDED after MAX-ITERATIONS iterations, if LIST has more than MAX-ITERATIONS elements."
  (let ((declarations
         (loop
            :while (and (consp (car body))
                        (eq 'declare (caar body)))
            :collect (pop body))))
    (with-gensyms (iterations)
      `(let ((,iterations 0))
         (dolist (,var ,list ,result)
           ,@declarations
           (when (> (incf ,iterations) ,bound)
             (error 'bound-exceeded))
           ,@body)))))

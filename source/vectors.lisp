;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/vectors
  (:use :clean-util/source/loops
        :clean-util/source/macros
        :clean-util/source/symbols
        :common-lisp)
  (:export :displaced
           :dltrim
           :dovector
           :drtrim
           :dtrim
           :ltrim
           :prefix-vector-p
           :rtrim
           :trim))

(in-package :clean-util/source/vectors)

(defun displaced (vector start &optional (end (length vector)))
  "Create and return a vector displaced to VECTOR or its base, if necessary.

Parameters:

VECTOR (VECTOR)
START ((INTEGER 0))
END ((INTEGER 0))

Description:

If START is 0 and END is (LENGTH VECTOR) then return VECTOR. Otherwise, return a
vector displaced to VECTOR or VECTOR's base, if VECTOR is displaced itself,
starting at index start of VECTOR and ending at index END of vector. If END is
not provided, it defaults to (LENGTH VECTOR)."
  (if (and (= start 0)
           (= end (length vector)))
      vector
      (multiple-value-bind (base offset)
          (array-displacement vector)
        (let ((offset (if base offset 0))
              (base (or base vector)))
          (make-array (- end start)
                      :element-type (array-element-type base)
                      :displaced-to base
                      :displaced-index-offset (+ offset start))))))

(defmacro dovector ((var vector &optional result) &body body)
  "Execute BODY once for each element of VECTOR.

Parameters:

VAR ((AND (NOT KEYWORD) SYMBOL))
VECTOR (FORM)
RESULT (FORM)
BODY (FORMS)

Description:

Evaluate VECTOR and execute BODY once for each element of the resulting
vector. The current element is lexically bound to VAR. Finally, evaluate
RESULT and return the result."
  (with-gensyms (gindex gvector)
    `(let ((,gvector ,vector))
       (dotimes (,gindex (length ,gvector) ,result)
         (let ((,var (aref ,gvector ,gindex)))
           ,@body)))))

(defun prefix-vector-p (prefix vector &optional (test #'equal))
  "Determine whether PREFIX is a prefix of VECTOR

Parameters:

PREFIX (VECTOR)
VECTOR (VECTOR)
TEST (FUNCTION)

Description:

Determine whether PREFIX is a prefix of VECTOR. Equality is tested with TEST."
  (and (>= (length vector) (length prefix))
       (funcall test prefix (displaced vector 0 (length prefix)))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %trim-docstring (displacedp leftp rightp with-limit)
    (concatenate 'string
                 "Trim WHAT from VECTOR"
                 (cond
                   ((and leftp rightp) "")
                   (leftp " from the left")
                   (rightp " from the right")
                   (t ""))
                 " and return the result "
                 (if displacedp
                     "displaced to VECTOR."
                     "as a new vector.")
                 "

Parameters:

VECTOR (VECTOR)
WHAT (T)
"
                 (if with-limit
                     "LIMIT ((INTEGER 0))
"
                     "")
                 "
Description:

Trim WHAT from VECTOR"
                 (cond
                   ((and leftp rightp) "")
                   (leftp " from the left")
                   (rightp " from the right")
                   (t ""))
                 ".

If WHAT is a function it is used as a predicate to determine what type of
elements to trim. Otherwise, elements EQ to WHAT are trimmed."
                 (if with-limit
                     "

At most LIMIT elements are trimmed of. LIMIT defaults to (LENGTH VECTOR)."
                     "")
                 "

The result is returned as "
                 (if displacedp
                     "a displaced vector relative to VECTOR."
                     "a new vector"))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %trim-function (name test displacedp leftp rightp with-limit)
    `(defun ,name (vector what ,@(when with-limit '(limit)))
       (let ((start 0)
             (end (length vector)))
         ,@(when leftp
             `((while (and ,@(when with-limit
                               '((< start limit)))
                           (< start end)
                           ,@(funcall test 'start))
                 (incf start))))
         ,@(when rightp
             `((while (and ,@(when with-limit
                               '((> end (- (length vector) limit))))
                           (> end start)
                           ,@(funcall test '(1- end)))
                 (decf end))))
         (,(if displacedp 'displaced 'subseq)
           vector start end)))))

(macrolet
    ((trim-functions (displacedp leftp rightp)
       (let* ((name (apply #'symbolicate
                           (append (when displacedp
                                     '(d))
                                   (cond
                                     ((and leftp (not rightp)) '(l))
                                     ((and rightp (not leftp)) '(r)))
                                   '(trim))))
              (predicate-name (symbolicate '% name '-by-predicate))
              (element-name (symbolicate '% name '-by-element))
              (with-limit (not (eq (not leftp) (not rightp)))))
         `(progn
            ,(%trim-function predicate-name
                             (lambda (index)
                               `((funcall what (aref vector ,index))))
                             displacedp leftp rightp with-limit)
            ,(%trim-function element-name
                             (lambda (index)
                               `((eq what (aref vector ,index))))
                             displacedp leftp rightp with-limit)
            (defun ,name (vector what ,@(when with-limit '(&key limit)))
              ,(%trim-docstring displacedp leftp rightp with-limit)
              ,(when with-limit '(declare (type (integer 0) limit)))
              (let (,@(when with-limit
                        '((limit (if limit
                                     (min limit (length vector))
                                     (length vector))))))
                (typecase what
                  (function (,predicate-name vector what ,@(when with-limit
                                                             '(limit))))
                  (otherwise (,element-name vector what ,@(when with-limit
                                                            '(limit)))))))
            (define-compiler-macro ,name (vector what ,@(when with-limit '(&key limit)))
              (with-gensyms (gvector)
                `(let ((,gvector ,vector))
                   ,(let (,@(when with-limit
                              '((limit (if limit
                                           `(min ,limit (length ,gvector))
                                           `(length ,gvector))))))
                      `(,(if (and (consp what) (member (car what)
                                                       '(function lambda)))
                             ',predicate-name
                             ',element-name)
                         ,gvector ,what ,,@(when with-limit '(limit)))))))))))
  (trim-functions t t t)
  (trim-functions t t nil)
  (trim-functions t nil t)
  (trim-functions nil t t)
  (trim-functions nil t nil)
  (trim-functions nil nil t))

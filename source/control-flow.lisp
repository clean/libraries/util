;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/control-flow
  (:use :clean-util/source/functions
        :clean-util/source/macros
        :common-lisp)
  (:export :switch))

(in-package :clean-util/source/control-flow)

;; I was going to write a STRING-CASE macro, but then read about SWITCH
;; in the alexandria library. Since SWITCH is more general, I implemented
;; it.
(defmacro switch ((keyform &key (test #'eql)) &body cases)
  "Conditionally execute forms based on key with test function.

Parameters:

KEYFORM (FORM)
TEST (FUNCTION)
CASES (FORMS)

Description:

SWITCH is an extension of CASE. It has an additional keyword parameter TEST,
which takes a function argument. The argument to TEST is used to compare KEYFORM
against the first element of each case in CASES, in that order. The default test
function is EQL, which results in the same behavior as CASE."
  (with-gensyms (test-function)
    (labels ((rec (cases)
               (when cases
                 (destructuring-bind (keys &body forms) (car cases)
                   (let ((keys (if (consp keys) keys (list keys))))
                     `(if (some ,test-function ',keys)
                          (progn ,@forms)
                          ,(rec (cdr cases))))))))
      `(let ((,test-function (curry ,test ,keyform)))
         ,(rec cases)))))

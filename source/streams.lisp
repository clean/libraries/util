;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/streams
  (:use :clean-util/source/bindings
        :clean-util/source/macros
        :common-lisp)
  (:export :dolines
           :eof-p
           :read-to-string))

(in-package :clean-util/source/streams)

(defmacro dolines ((line stream &optional result) &body body)
  "Execute BODY once for each line read from STREAM.

Parameters:

LINE ((AND (NOT KEYWORD) SYMBOL))
STREAM (FORM)
RESULT (FORM)
BODY (FORMS)

Description:

Evaluate STREAM and execute BODY once for each successful call to READ-LINE on
the resulting stream. The current line is lexically bound to LINE. Finally,
evaluate RESULT and return the result."
  (with-gensyms (evaluated-stream)
    `(let ((,evaluated-stream ,stream))
       (let-while (,line (read-line ,evaluated-stream nil))
         ,@body)
       ,result)))

(defun eof-p (stream)
  "Return true if and only if STREAM is at the end-of-file position.

Parameters:

STREAM (STREAM)"
  (eq :eof (peek-char nil stream nil :eof)))

(defun read-to-string (stream)
  "Reads the remainder of characters from STREAM and returns them as a string.

Parameters:

STREAM (STREAM)

Errors:

SIMPLE-ERROR if STREAM is not a valid argument to READ-CHAR."
  (with-output-to-string (output-stream)
    (let-while (character (read-char stream nil nil))
      (write-char character output-stream))))

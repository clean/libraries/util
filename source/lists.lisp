;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/lists
  (:use :common-lisp)
  (:export :nconc1
           :nconc1f))

(in-package :clean-util/source/lists)

(defun nconc1 (list element)
  "Destructively add ELEMENT to the end of LIST

Parameters:

LIST (LIST)
ELEMENT (T)

Description:

Destructively add ELEMENT to the end of LIST and return the result."
  (nconc list (list element)))

(define-modify-macro nconc1f (element) nconc1
                     "Destructively add ELEMENT to the end of a list at PLACE

Parameters:

PLACE ((PLACE LIST))
ELEMENT (T)

Description:

Destructively add ELEMENT to the end of the list at PLACE and bind PLACE to the
result.")

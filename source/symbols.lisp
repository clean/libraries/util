;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/symbols
  (:use :common-lisp)
  (:export :symbolicate))

(in-package :clean-util/source/symbols)

(defun symbolicate (&rest string-designators)
  "SYMBOLICATE turns STRING-DESIGNATORS into uppercased version of the strings
 they represents. The results are concatenated in order and a symbol named after
 the result is interned in the current package."
  (intern
   (string-upcase
    (apply #'concatenate 'string
           (mapcar #'string string-designators)))))

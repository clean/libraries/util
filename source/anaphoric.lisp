;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/anaphoric
  (:use :clean-util/source/symbols
        :common-lisp)
  (:export :acase
           :acond
           :aecase
           :aetypecase
           :aif
           :atypecase
           :it))

(in-package :clean-util/source/anaphoric)

(defmacro %define-anaphoric ((form operator) &body body)
  `(let ((it ,form))
     (,operator it ,@body)))

(defmacro aif (test-form then-form &optional else-form)
  "Anaphoric IF

Description:

Works like IF while the result of TEST-FORM is lexically bound to IT in the THEN-FORM and ELSE-FORM."
  `(%define-anaphoric (,test-form if)
     ,then-form
     ,else-form))

(defmacro acond (&rest clauses)
  "Anaphoric COND

Description:

Works like COND while the result of a successful test form is lexically bound to IT in the forms of the clause."
  (when clauses
    (destructuring-bind ((predicate &rest body) . rest) clauses
      `(aif ,predicate
            (progn ,@body)
            (acond ,@rest)))))

(macrolet ((define-anaphoric-case (operator)
  `(defmacro ,(symbolicate "A" operator) (keyform &body cases)
     ,(format nil "Anaphoric ~A

Description:

Works like ~A while the result of KEYFORM is lexically bound to IT in the forms of the clauses."
              operator
              operator)
     `(%define-anaphoric (,keyform ,',operator)
        ,@cases))))
  (define-anaphoric-case case)
  (define-anaphoric-case ecase)
  (define-anaphoric-case typecase)
  (define-anaphoric-case etypecase))

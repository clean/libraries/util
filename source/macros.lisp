;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/macros
  (:use :clean-util/source/symbols
        :common-lisp)
  (:export :once-only
           :with-gensyms))

(in-package :clean-util/source/macros)

(defmacro with-gensyms ((&rest symbols) &body body)
  "Binds for each symbol SYMBOL in SYMBOLS a new uninterned symbol to SYMBOL.

The uninterned symbol for SYMBOL has SYMBOL's name suffixed by a decimal integer
as its name."
  `(let ,(loop :for s :in symbols :collect `(,s (gensym ,(string s))))
     ,@body))

(defmacro once-only ((&rest symbols) &body body)
  "Ensures that forms bound to SYMBOLS are only evaluated once.

Generates code that evaluates forms bound to SYMBOLS and binds the result to gensyms. Within BODY the symbols SYMBOLS are bound to these gensyms."
  (let ((gensyms (loop :for s :in symbols :collect (gensym (string s)))))
    `(with-gensyms ,gensyms
       `(let (,,@(loop :for s :in symbols :for g :in gensyms :collect ``(,,g ,,s)))
          ,(let ,(loop :for s :in symbols :for g :in gensyms :collect `(,s ,g))
             ,@body)))))

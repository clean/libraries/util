;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/bindings
  (:use :clean-util/source/loops
        :clean-util/source/symbols
        :common-lisp)
  (:export :let-case
           :let-ecase
           :let-typecase
           :let-etypecase
           :let-if
           :let-return
           :let-when
           :let-while))

(in-package :clean-util/source/bindings)

(defmacro let-return ((var &optional form) &body body)
  "Evaluates FORM, binds the result to VAR, evaluates BODY and returns the value of VAR.

Parameters:

VAR (SYMBOL)
FORM (FORM)
BODY (FORMS)"
  (check-type var symbol)
  `(let ((,var ,form))
     ,@body
     ,var))

(defmacro let-when ((var predicate) &body body)
  "Conditionally evaluate a body of code with the condition's result bound to a local variable.

Parameters:

VAR (SYMBOL)
PREDICATE (FORM)
BODY (FORMS)

Description:

Evaluate PREDICATE. If it returns NIL return NIL. Otherwise, execute BODY with PREDICATE's value lexically bound to VAR and return BODY's return value."
  `(let ((,var ,predicate))
     (when ,var ,@body)))

(defmacro let-if ((var predicate) then &optional else)
  "Evaluate PREDICATE and bind its result to VAR for the evaluation of THEN or ELSE.

Parameters:

VAR (SYMBOL)
PREDICATE (FORM)
THEN (FORM)
ELSE (FORM)

Description:

Evaluate PREDICATE and bind the result to VAR. Within the context of this binding, if PREDICATE evaluated to true, execute THEN and return its result, otherwise execute ELSE and return its result."
  `(let ((,var ,predicate))
     (if ,var ,then ,else)))

(defmacro let-while ((var predicate) &body body)
  "Evaluate BODY with PREDICATE's result bound to VAR as long as PREDICATE evaluates to true.

Parameters:

VAR (SYMBOL)
PREDICATE (FORM)
BODY (FORMS)

Description:

Alternately evaluate PREDICATE and BODY starting with PREDICATE as long as evaluating PREDICATE returns true. For each evaluation of BODY, VAR is bound to the result of the previous evaluation of PREDICATE. Return NIL."
  `(let ((,var))
     (while (setf ,var ,predicate)
       ,@body)))

(macrolet ((define-binding-case (operator)
             `(defmacro ,(symbolicate "LET-" operator) ((var keyform) &body cases)
                (format nil "Like CASE with KEYFORM's result lexically bound to VAR

Parameters:

VAR (SYMBOL)
KEYFORM (FORM)
CASES (FORMS)")
                `(let ((,var ,keyform))
                   (,',operator ,var ,@cases)))))
  (define-binding-case case)
  (define-binding-case ecase)
  (define-binding-case typecase)
  (define-binding-case etypecase))

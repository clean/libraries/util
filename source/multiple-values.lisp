;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/multiple-values
  (:use :clean-util/source/macros
        :common-lisp)
  (:export :mv-or))

(in-package :clean-util/source/multiple-values)

(defmacro mv-or (&rest clauses)
  "Multiple Value OR

Parameters:

CLAUSES (FORMS)

Description:

Works like OR with the exception that every clause, and not just the last, can
return multiple values."
  (when clauses
    (with-gensyms (result)
      `(let ((,result (multiple-value-list ,(car clauses))))
         (if (car ,result)
             (values-list ,result)
             (mv-or ,@(cdr clauses)))))))

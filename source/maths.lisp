;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/maths
  (:use :clean-util/source/macros
        :clean-util/source/symbols
        :common-lisp)
  (:export :/2
           :rounddown
           :roundup))

(in-package :clean-util/source/maths)

(declaim (inline /2))
(defun /2 (number)
  "Divide NUMBER by 2.

Parameters:

NUMBER (NUMBER)"
  (/ number 2))

(macrolet ((defround (direction function documentation-direction)
             (let ((name (symbolicate 'round direction)))
               `(progn
                  (declaim (inline ,name))
                  (defun ,name (amount step-size)
                    ,(format nil "Round AMOUNT to the next ~A multiple of STEP-SIZE.

Parameters:

AMOUNT (REAL)
STEP-SIZE (REAL)"
                             documentation-direction)
                    (* (,function amount step-size) step-size))
                  (define-compiler-macro ,name (amount step-size)
                    (with-gensyms (gstep-size)
                      `(let ((,gstep-size ,step-size))
                         (* (,',function ,amount ,gstep-size) ,gstep-size))))))))
  (defround up ceiling "higher")
  (defround down floor "lower"))

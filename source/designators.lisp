;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/designators
  (:use :common-lisp)
  (:export :package-designator
           :string-designator))

(in-package :clean-util/source/designators)

(deftype string-designator ()
  "STRING-DESIGNATOR is a type corresponding to the informal type package-designator as described in the glossary of the HyperSpec"
  '(or character symbol string))

(deftype package-designator ()
  "PACKAGE-DESIGNATOR is a type corresponding to the informal type package-designator as described in the glossary of the HyperSpec"
  '(or package string-designator))

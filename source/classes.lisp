;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/classes
  (:use :common-lisp)
  (:export :missing-initarg
           :missing-initarg-error
           :missing-initarg-error-class
           :missing-initarg-error-slot-name))

(in-package :clean-util/source/classes)

(defun %missing-initarg-error-reporter (error stream)
  (format stream
          "Instances of class ~a require an initialization argument for slot ~a."
          (missing-initarg-error-class error)
          (missing-initarg-error-slot-name error)))

(define-condition missing-initarg-error (error)
  ((%slot-name :initarg :slot-name
               :reader missing-initarg-error-slot-name
               :type symbol)
   (%class :initarg :class
           :reader missing-initarg-error-class
           :type symbol))
  (:report %missing-initarg-error-reporter)
  (:documentation
   "Error condition signaled by MISSING-ARG."))

(defun missing-initarg (slot-name class)
  "Signals a MISSING-INITARG-ERROR error condition with slots SLOT-NAME and
 CLASS bound to the values of the correspondingly named arguments."
  (check-type slot-name symbol)
  (check-type class symbol)
  (error 'missing-initarg-error :slot-name slot-name :class class))

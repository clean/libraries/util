;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/source/hash-tables
  (:use :clean-util/source/macros
        :common-lisp)
  (:export :dokeys
           :dovalues
           :hash-table-keys
           :hash-table-values
           :mapkeys
           :mapvalues
           :put-new))

(in-package :clean-util/source/hash-tables)

(defmacro dokeys ((var hash-table &optional result) &body body)
  "Execute BODY once for each key of HASH-TABLE.

Parameters:

VAR ((AND (NOT KEYWORD) SYMBOL))
HASH-TABLE (FORM)
RESULT (FORM)
BODY (FORMS)

Description:

Evaluate HASH-TABLE and execute BODY once for each key of the resulting hash
table. The current key is lexically bound to VAR. Finally, evaluate RESULT and
return the result."
  (with-gensyms (gignored)
    `(progn
       (maphash (lambda (,var ,gignored)
                  (declare (ignore ,gignored))
                  ,@body)
                ,hash-table)
       ,result)))

(defmacro dovalues ((var hash-table &optional result) &body body)
  "Execute BODY once for each value of HASH-TABLE.

Parameters:

VAR ((AND (NOT KEYWORD) SYMBOL))
HASH-TABLE (FORM)
RESULT (FORM)
BODY (FORMS)

Description:

Evaluate HASH-TABLE and execute BODY once for each value of the resulting hash
table. The current value is lexically bound to VAR. Finally, evaluate RESULT and
return the result."
  (with-gensyms (gignored)
    `(progn
       (maphash (lambda (,gignored ,var)
                  (declare (ignore ,gignored))
                  ,@body)
                ,hash-table)
       ,result)))

(defun mapkeys (function table)
  "MAPKEYS iterates over the keys of hash table TABLE. It applies FUNCTION to each
key and collects the returned values in a list. That list is returned."
  (loop
     :for value :being :the :hash-keys :of table
     :collect (funcall function value)))

(defun mapvalues (function table)
  "MAPVALUES iterates over the values of hash table TABLE. It applies FUNCTION to
each value and collects the returned values in a list. That list is returned."
  (loop
     :for value :being :the :hash-values :of table
     :collect (funcall function value)))

(defun hash-table-keys (table)
  "Return the keys of hash table TABLE as a list."
  (mapkeys #'identity table))

(defun hash-table-values (table)
  "Return the values of hash table TABLE as a list."
  (mapvalues #'identity table))

(defun put-new (table key value)
  "Associate VALUE with KEY in TABLE unless KEY is already a key of TABLE

Parameters:

TABLE (HASH-TABLE)
KEY (T)
VALUE (T)

Description:

Associate VALUE with KEY in TABLE unless there is already a value associated
with KEY. If KEY is already associated with a value that value is returned. Otherwise, VALUE is returned."
  (let ((previous (gethash key table)))
    (or previous
        (setf (gethash key table) value))))

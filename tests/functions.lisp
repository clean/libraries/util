;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/functions
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/functions)

(define-test-suite (functions :parent clean-util))

(with-test-suite (curry :parent functions)
  (define-test-case (single-argument)
    (assert-that (= 0 (funcall (curry #'+ -1) 1)))))

(with-test-suite (rcurry :parent functions)
  (define-test-case (single-argument)
    (assert-that (= 2 (funcall (rcurry #'- -1) 1)))))

(with-test-suite (add-ignored-arguments :parent functions)
  (define-test-case (no-arguments)
    (assert-that (= 0 (funcall (add-ignored-arguments (constantly 0) 0 0 0)))))

  (define-test-case (no-ignored-arguments)
    (assert-that (= 0 (funcall (add-ignored-arguments (lambda (x) x) 0 1 0) 0))))

  (define-test-case (front-arguments-ignored)
    (assert-that (= 0 (funcall (add-ignored-arguments (constantly 0) 1 0 0) 1))))

  (define-test-case (no-style-warning-with-additional-args)
    (assert-that (handler-case
                     (compile nil '(lambda ()
                                    (add-ignored-arguments #'+ 1 0 0)))
                   (style-warning () nil))))

  (define-test-case (back-arguments-ignored)
      (assert-that (= 0 (funcall (add-ignored-arguments (constantly 0) 0 0 1) 1))))

  (define-test-case (front-and-back-arguments-ignored)
      (assert-that (= 0 (funcall (add-ignored-arguments (constantly 0) 1 0 1) 1 2)))))

;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/multiple-values
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/multiple-values)

(define-test-suite (multiple-values :parent clean-util))

(with-test-suite (mv-or :parent multiple-values)

  (define-test-case (first-of-two)
    (multiple-value-bind (first second)
        (mv-or (values 1 2) 'foo)
      (assert-that (= 1 first))
      (assert-that (= 2 second))))

  (define-test-case (second-of-three)
    (multiple-value-bind (first second)
        (mv-or (values nil 2) (values 1 2) 'foo)
      (assert-that (= 1 first))
      (assert-that (= 2 second)))))

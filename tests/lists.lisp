;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/lists
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/lists)

(define-test-suite (lists :parent clean-util))

(with-test-suite (nconc1 :parent lists)

  (define-test-case (from-empty)
    (assert-that (equal '(1) (nconc1 nil 1))))

  (define-test-case (not-empty)
    (assert-that (equal '(1 2) (nconc1 (list 1) 2)))))

(with-test-suite (nconc1f :parent lists)

  (define-test-case (from-empty)
    (let ((list nil))
      (nconc1f list 1)
      (assert-that (equal '(1) list))))

  (define-test-case (not-empty)
    (let ((list (list 1)))
      (nconc1f list 2)
      (assert-that (equal '(1 2) list)))))

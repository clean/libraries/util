;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/macros
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/macros)

(define-test-suite (macros :parent clean-util))

(with-test-suite (with-gensyms :parent macros)
  (define-test-case (with-gensyms)
    (with-gensyms (a b c)
      (assert-that (and (not (eq a b))
                        (not (eq b c))
                        (not (eq c a))))
      (assert-that (char= (aref (string a) 0) #\A)))))

(defmacro %foo (bar)
  (once-only (bar)
    `(let ((a ,bar)
           (b ,bar))
       (+ a b))))

(with-test-suite (once-only :parent macros)
  (define-test-case (once-only)
    (let ((a 0))
      (assert-that (= 2 (%foo (incf a)))))))

;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/anaphoric
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/anaphoric)

(define-test-suite (anaphoric :parent clean-util))

(with-test-suite (acond :parent anaphoric)

  (define-test-case (no-clause)
    (assert-that (null (acond))))

  (define-test-case (one-clause-hit)
    (assert-that (= 1 (acond
                       (1 it)))))

  (define-test-case (one-clause-no-hit)
    (assert-that (null (acond
                        ((> 2 3) it)))))

  (define-test-case (two-clauses)
    (assert-that (= 1 (acond
                       ((> 2 3) it)
                       (1 it))))))

(with-test-suite (aif :parent anaphoric)

  (define-test-case (true-statement)
    (assert-that (= 1 (aif 1 it))))

  (define-test-case (false-statement)
    (assert-that (null (aif nil 1 it)))))

(with-test-suite (acase :parent anaphoric)

  (define-test-case (first-clause)
    (assert-that (= 1 (acase 7
                        ((2 3 5 7) (- it 6))
                        (otherwise it)))))

  (define-test-case (otherwise-clause)
    (assert-that (= 1 (acase 1
                        ((2 3 5 7) (- it 6))
                        (otherwise it))))))

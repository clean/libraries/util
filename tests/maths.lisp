;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/maths
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/maths)

(define-test-suite (maths :parent clean-util))

(with-test-suite (/2 :parent maths)
  (define-test-case (half-some-numbers)
    (dotimes (i 100)
      (let ((number (random most-positive-fixnum)))
        (assert-that (= (/ number 2) (/2 number)))))))

(with-test-suite (roundup :parent maths)

  (define-test-case (positive)
    (assert-that (= 20 (roundup 10 20))))

  (define-test-case (negative)
    (assert-that (= 0 (roundup -10 20))))

  (define-test-case (float)
    (assert-that (= 42.0 (roundup 35 10.5)))))

(with-test-suite (rounddown :parent maths)

  (define-test-case (positive)
    (assert-that (= 0 (rounddown 10 20))))

  (define-test-case (negative)
    (assert-that (= -20 (rounddown -10 20))))

  (define-test-case (float)
    (assert-that (= 31.5 (rounddown 35 10.5)))))

;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/classes
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/classes)

(define-test-suite (classes :parent clean-util))

(with-test-suite (missing-initarg :parent classes)

  (define-test-case (signals-missing-initarg-error)
    (assert-error (missing-initarg-error)
                  (missing-initarg 'slot 'class)))

  (define-test-case (passed-arguments-are-stored-properly)
    (handler-case (missing-initarg 'slot 'class)
      (missing-initarg-error (error)
        (assert-that (eql (missing-initarg-error-slot-name error) 'slot))
        (assert-that (eql (missing-initarg-error-class error) 'class)))))

  (define-test-case (passed-arguments-must-be-symbols)
    (assert-error (type-error)
                  (missing-initarg "slot" 'class))
    (assert-error (type-error)
                  (missing-initarg 'slot "class"))))

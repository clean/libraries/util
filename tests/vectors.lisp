;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/vectors
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/vectors)

(define-test-suite (vectors :parent clean-util))

(with-test-suite (displaced :parent vectors)

  (define-test-case (mutating-the-base)
    (let* ((string (copy-seq "bar"))
           (displaced (displaced string 1)))
      (assert-that (eq #\r (elt displaced 1)))
      (setf (elt string 2) #\z)
      (assert-that (eq #\z (elt displaced 1)))))

  (define-test-case (do-nothing)
    (let* ((base "foo")
           (displaced (displaced base 0)))
      (assert-that (eq base displaced))))

  (define-test-case (use-base-of-vector)
    (let* ((base "foobar")
           (level1 (displaced base 1))
           (level2 (displaced level1 1)))
      (multiple-value-bind (level2-base offset)
          (array-displacement level2)
        (assert-that (eq level2-base base))
        (assert-that (= 2 offset))))))

(with-test-suite (dovector :parent vectors)

  (define-test-case (zero-elements)
    (assert-that
     (zerop
      (let ((i 0))
        (dovector (character "" i)
          (incf i (char-code character)))))))

  (define-test-case (one-element)
    (assert-that
     (= 50
      (let ((i 0))
        (dovector (character "2" i)
          (incf i (char-code character)))))))

  (define-test-case (two-elements)
    (assert-that
     (= 100
      (let ((i 0))
        (dovector (character "22" i)
          (incf i (char-code character))))))))

(with-test-suite (prefix-vector-p :parent :vectors)

  (define-test-case (prefix-longer)
    (assert-that (not (prefix-vector-p "asdf" "as"))))

  (define-test-case (does-not-match)
    (assert-that (not (prefix-vector-p "asdf" "bsdf"))))

  (define-test-case (equal)
    (assert-that (prefix-vector-p "asdf" "asdf")))

  (define-test-case (match-not-equal)
    (assert-that (prefix-vector-p "asdf" "asdfg")))

  (define-test-case (other-test-function)
    (assert-that (prefix-vector-p "ASDF" "asdf" #'string-equal))))

(defmacro %trim-element-test-case ((testee) &body results)
  `(define-test-case (element)
     ,@(mapcar (lambda (result source)
                 `(assert-that (string= ,result (,testee ,source #\Space))))
               results
               '("foo" " foo" "  foo" "foo " "foo  " " foo " "  foo  " "  foo "
                 "f o"))))

(defmacro %trim-predicate-test-case ((testee) &body results)
  `(define-test-case (predicate)
     ,@(mapcar (lambda (result source)
                 `(assert-that (string= ,result
                                        (,testee ,source
                                                 (lambda (c) (find c "bar"))))))
               results
               '("foo" "barfoo" "foobar" "barfoobar" "foobarfoo"))))

(defmacro %trim-limit-test-case (testee)
  `(define-test-case (limit)
     (assert-that (string= "aaa" (,testee "aaa" #\a :limit 0)))
     (assert-that (string= "aa" (,testee "aaa" #\a :limit 1)))
     (assert-that (string= "a" (,testee "aaa" #\a :limit 2)))
     (assert-that (string= "" (,testee "aaa" #\a :limit 3)))
     (assert-that (string= "" (,testee "aaa" #\a :limit 4)))))

(with-test-suite (ltrim :parent vectors)

  (%trim-element-test-case (ltrim)
    "foo" "foo" "foo" "foo " "foo  " "foo " "foo  " "foo " "f o")

  (%trim-predicate-test-case (ltrim)
    "foo" "foo" "foobar" "foobar" "foobarfoo")

  (%trim-limit-test-case ltrim))

(with-test-suite (rtrim :parent vectors)

  (%trim-element-test-case (rtrim)
    "foo" " foo" "  foo" "foo" "foo" " foo" "  foo" "  foo" "f o")

  (%trim-predicate-test-case (rtrim)
    "foo" "barfoo" "foo" "barfoo" "foobarfoo")

  (%trim-limit-test-case rtrim))

(with-test-suite (trim :parent vectors)

  (%trim-element-test-case (trim)
    "foo" "foo" "foo" "foo" "foo" "foo" "foo" "foo" "f o")

  (%trim-predicate-test-case (trim)
    "foo" "foo" "foo" "foo" "foobarfoo"))

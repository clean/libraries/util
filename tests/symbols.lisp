;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/symbols
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/symbols)

(define-test-suite (symbols :parent clean-util))

(with-test-suite (symbolicate :parent symbols)

  (defmacro test-symbolicate (input name)
    `(let ((sym (symbolicate ,input)))
       (assert-that sym)
       (unwind-protect
            (progn
              (assert-that (typep sym 'symbol))
              (assert-that (find-symbol ,name)))
         (unintern sym))))

  (define-test-case (characters-work)
    (test-symbolicate #\a "A"))

  (define-test-case (symbols-work)
    (test-symbolicate 'a "A"))

  (define-test-case (strings-work)
    (test-symbolicate "a" "A")))

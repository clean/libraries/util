;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/hash-tables
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/hash-tables)

(define-test-suite (hash-tables :parent clean-util))

(defmacro with-n-element-table ((table n) &body body)
  (let ((i (gensym "i")))
    `(let ((,table (make-hash-table)))
       (dotimes (,i ,n) (setf (gethash ,i ,table) ,i))
       ,@body)))

(with-test-suite (dokeys :parent hash-tables)

  (define-test-case (no-element)
    (assert-that
     (zerop
      (let ((i 0))
        (with-n-element-table (table 0)
          (dokeys (key table i)
            (incf i (1+ key))))))))

  (define-test-case (one-element)
    (assert-that
     (eq 1
         (let ((i 0))
           (with-n-element-table (table 1)
             (dokeys (key table i)
               (incf i (1+ key))))))))

  (define-test-case (two-elements)
    (assert-that
     (eq 3
         (let ((i 0))
           (with-n-element-table (table 2)
             (dokeys (key table i)
               (incf i (1+ key)))))))))

(with-test-suite (dovalues :parent hash-tables)

  (define-test-case (no-element)
    (assert-that
     (zerop
      (let ((i 0))
        (with-n-element-table (table 0)
          (dovalues (value table i)
            (incf i (1+ value))))))))

  (define-test-case (one-element)
    (assert-that
     (eq 1
         (let ((i 0))
           (with-n-element-table (table 1)
             (dovalues (value table i)
               (incf i (1+ value))))))))

  (define-test-case (two-elements)
    (assert-that
     (eq 3
         (let ((i 0))
           (with-n-element-table (table 2)
             (dovalues (value table i)
               (incf i (1+ value)))))))))

(with-test-suite (mapkeys :parent hash-tables)
  (define-test-case (map-over-hash-table)
    (with-n-element-table (table 3)
      (let ((mapped (mapkeys #'identity table)))
        (assert-that (every (rcurry #'member mapped) '(0 1 2)))))))

(with-test-suite (mapvalues :parent hash-tables)
  (define-test-case (map-over-hash-table)
    (with-n-element-table (table 3)
      (let ((mapped (mapvalues #'identity table)))
        (assert-that (every (rcurry #'member mapped) '(0 1 2)))))))

(with-test-suite (hash-table-keys :parent hash-tables)
  (define-test-case (hash-table-keys)
    (with-n-element-table (table 3)
      (let ((mapped (hash-table-keys table)))
        (assert-that (every (rcurry #'member mapped) '(0 1 2)))))))

(with-test-suite (hash-table-values :parent hash-tables)
  (define-test-case (hash-table-values)
    (with-n-element-table (table 3)
      (let ((mapped (hash-table-values table)))
        (assert-that (every (rcurry #'member mapped) '(0 1 2)))))))

(with-test-suite (put-new :parent hash-tables)

  (define-test-case (once)
    (let ((table (make-hash-table)))
      (assert-that (eq 'bar (put-new table 'foo 'bar)))
      (assert-that (= 1 (hash-table-count table)))))

  (define-test-case (twice)
    (let ((table (make-hash-table)))
      (assert-that (eq 'bar (put-new table 'foo 'bar)))
      (assert-that (eq 'bar (put-new table 'foo 'baz)))
      (assert-that (= 1 (hash-table-count table))))))

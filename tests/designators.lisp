;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/designators
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/designators)

(define-test-suite (designators :parent clean-util))

(with-test-suite (string-designator :parent designators)

  (define-test-case (characters-are-string-designators)
    (assert-that (typep #\a 'string-designator)))

  (define-test-case (symbols-are-string-designators)
    (assert-that (typep 'a 'string-designator)))

  (define-test-case (strings-are-string-designators)
    (assert-that (typep "a" 'string-designator))))

(with-test-suite (package-designator :parent designators)

  (define-test-case (string-designators-are-package-designators)
    (assert-that (typep #\a 'package-designator))
    (assert-that (typep 'a 'package-designator))
    (assert-that (typep "a" 'package-designator)))

  (define-test-case (packages-are-package-designators)
    (assert-that (typep *package* 'package-designator))))

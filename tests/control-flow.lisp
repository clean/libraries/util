;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/control-flow
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/control-flow)

(define-test-suite (control-flow :parent clean-util))

(with-test-suite (switch :parent control-flow)

  (define-test-case (no-forms-is-nil)
    (assert-that (null (switch ("HI")))))

  (define-test-case (default-is-eql)
    (let ((hi (string 'hi)))
      (assert-that (null (switch (hi)
                           ("HI" T))))))

  (define-test-case (non-symmetric-function)
    (assert-that (switch (3 :test #'<)
                         (4 T)))
    (assert-that (null (switch (3 :test #'>)
                         (4 T)))))

  (define-test-case (equal-test-function)
    (let ((hi (string 'hi)))
      (assert-that (switch (hi :test #'equal)
                     ("HI" t))))
    (let ((vec (make-array 3 :initial-element 0)))
      (assert-that (null (switch (vec :test #'equal)
                           (#(0 0 0) t))))))

  (define-test-case (equalp-test-function)
    (let ((vec (make-array 3 :initial-element 0)))
      (assert-that (switch (vec :test #'equalp)
                     (#(0 0 0) t)))))

  (define-test-case (more-than-one-form-in-body)
    (assert-that (switch ("foo" :test #'string=)
                   ("foo" nil t)
                   ("bar" t nil)))))

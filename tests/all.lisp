;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/all
  (:use :clean-util/tests/suite

        :clean-util/tests/anaphoric
        :clean-util/tests/bindings
        :clean-util/tests/classes
        :clean-util/tests/control-flow
        :clean-util/tests/designators
        :clean-util/tests/functions
        :clean-util/tests/hash-tables
        :clean-util/tests/lists
        :clean-util/tests/loops
        :clean-util/tests/macros
        :clean-util/tests/maths
        :clean-util/tests/multiple-values
        :clean-util/tests/streams
        :clean-util/tests/symbols
        :clean-util/tests/vectors))

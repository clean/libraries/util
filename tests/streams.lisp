;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/streams
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/streams)

(define-test-suite (streams :parent clean-util))

(with-test-suite (dolines :parent streams)

  (define-test-case (zero-lines)
    (assert-that (zerop (let ((i 0))
                          (with-input-from-string (stream "")
                            (dolines (line stream i)
                              (incf i (parse-integer line))))))))

  (define-test-case (one-line)
    (assert-that (zerop (let ((i -1))
                          (with-input-from-string (stream (format nil "1~%"))
                            (dolines (line stream i)
                              (incf i (parse-integer line))))))))

  (define-test-case (two-lines)
    (assert-that (zerop (let ((i -2))
                          (with-input-from-string (stream
                                                   (format nil "1~%1~%"))
                            (dolines (line stream i)
                              (incf i (parse-integer line)))))))))

(with-test-suite (eof-p :parent streams)

  (define-test-case (empty-stream)
    (with-input-from-string (stream "")
      (assert-that (eof-p stream))))

  (define-test-case (non-empty-stream-beginning)
    (with-input-from-string (stream " ")
      (assert-that (not (eof-p stream)))))

  (define-test-case (non-empty-stream-end)
    (with-input-from-string (stream " ")
      (read-char stream)
      (assert-that (eof-p stream)))))

(with-test-suite (read-to-string :parent streams)

  (define-test-case (full-file)
    (with-open-file (stream "tests/read-to-string.test-input")
      (assert-that (string= (read-to-string stream) "(foo) bar
"))))

  (define-test-case (partial-file)
    (with-open-file (stream "tests/read-to-string.test-input")
      (read stream)
      (assert-that (string= (read-to-string stream) "bar
"))))

  (define-test-case (error)
    (assert-error (error) (read-to-string 1))))

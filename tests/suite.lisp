;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/suite
  (:use :common-lisp
        :clean-test))

(in-package :clean-util/tests/suite)

(define-test-suite (clean-util))

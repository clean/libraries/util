;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/loops
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/loops)

(define-test-suite (loops :parent clean-util))

(with-test-suite (while :parent loops)

  (define-test-case (body-evaluated)
    (assert-that (zerop (let-return (i 9)
                          (while (not (zerop i))
                            (decf i))))))

  (define-test-case (body-not-evaluated)
    (assert-that (null (let-return (foo)
                         (while nil
                           (setf foo 'bar)))))))

(with-test-suite (until :parent loops)

  (define-test-case (body-evaluated)
    (assert-that (zerop (let-return (i 9)
                          (until (zerop i)
                            (decf i))))))

  (define-test-case (body-not-evaluated)
    (assert-that (null (let-return (foo)
                         (until t
                           (setf foo 'bar)))))))

(with-test-suite (bounded-dolist :parent loops)

  (define-test-case (bound-higher-than-necessary)
    (assert-that (null (bounded-dolist (e '(1 2) 10)
                         (declare (ignore e))))))

  (define-test-case (bound-exactly-right)
    (assert-that (null (bounded-dolist (e '(1 2) 2)
                         (declare (ignore e))))))

  (define-test-case (bound-lower-than-necessary)
    (assert-error (bound-exceeded)
                  (bounded-dolist (e '(1 2) 1)
                    (declare (ignore e))))))

;;;; This file is a part of the Clean Util library.

(defpackage :clean-util/tests/bindings
  (:use :clean-test
        :clean-util
        :common-lisp))

(in-package :clean-util/tests/bindings)

(define-test-suite (bindings :parent clean-util))

(with-test-suite (let-return :parent bindings)

  (define-test-case (base)
    (assert-that (= 0 (let-return (foo 0) 1))))

  (define-test-case (setf-variable)
    (assert-that (= 1 (let-return (foo 0) (setf foo 1))))))

(with-test-suite (let-when :parent bindings)

  (define-test-case (expression-is-true)
    (assert-that (= 1 (let-when (a 1) a))))

  (define-test-case (expression-is-false)
    (assert-that (null (let-when (a nil) (+ 1 a))))))

(with-test-suite (let-if :parent bindings)

  (define-test-case (expression-is-true)
    (assert-that (= 1 (let-if (a 1) a a))))

  (define-test-case (expression-is-false)
    (assert-that (null (let-if (a nil) (+ 1 a) a)))))

(with-test-suite (let-while :parent bindings)

  (define-test-case (body-evaluated)
    (let ((g (gensym)))
      (assert-that (eq g
                       (block body-evaluated
                         (let-while (baz g)
                           (return-from body-evaluated baz)))))))

  (define-test-case (body-evaluated-twice)
    (let ((i 0))
      (with-input-from-string (stream (format nil "FOO~%BAR~%"))
        (let-while (line (read-line stream nil))
          (incf i)))
      (assert-that (= i 2))))

  (define-test-case (body-not-evaluated)
    (assert-that (null (block body-not-evaluated
                         (let-while (foo nil)
                           (return-from body-not-evaluated t)))))))

(with-test-suite (let-case :parent bindings)

  (define-test-case (first-clause)
    (assert-that (= 1 (let-case (it 7)
                        ((2 3 5 7) (- it 6))
                        (otherwise it)))))

  (define-test-case (otherwise-clause)
    (assert-that (= 1 (let-case (it 1)
                        ((2 3 5 7) (- it 6))
                        (otherwise it))))))

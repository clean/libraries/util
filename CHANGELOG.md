# Change Log

All notable changes to this project are documented in this file. This project
adheres to [Semantic Versioning](https://semver.org)

## [Unreleased]

### Added

- `ADD-IGNORED-ARGUMENTS` macro
- `BOUNDED-DOLIST` macro
- `EOF-P` function
- `PREFIX-VECTOR-P` function
- `READ-TO-STRING` function
- `SWITCH` macro

## [2.0.0] 2017-07-21

### Changed

- Lambda list of `LET-RETURN`

### Removed

- `DO-ELEMENTS` macro

## [1.4.0] 2017-07-20

### Added

- `ACASE` macro
- `ACOND` macro
- `AECASE` macro
- `AETYPECASE` macro
- `AIF` macro
- `ATYPECASE` macro
- `DISPLACED` function
- `DLTRIM` function
- `DOVECTOR` macro
- `DRTRIM` function
- `DTRIM` function
- `LET-CASE` macro
- `LET-ECASE` macro
- `LET-ETYPECASE` macro
- `LET-TYPECASE` macro
- `LTRIM` function
- `MV-OR` macro
- `NCONC1` function
- `NCONC1F` macro
- `ONCE-ONLY` macro
- `PUT-NEW` function
- `RTRIM` function
- `TRIM` function
- `UNTIL` macro

## [1.3.0] 2017-02-26

### Added

- `DO-ELEMENTS` macro
- `HASH-TABLE-KEYS` function
- `LET-IF` macro
- `MAPKEYS` function
- `ROUNDDOWN` function
- `ROUNDUP` function
- Optional `RESULT` parameter to `DOLINES`, `DOKEYS` and `DOVALUES`

## [1.2.0] 2017-02-16

### Added

- `DOLINES` macro
- `LET-WHILE` macro

## [1.1.0] 2017-02-09

### Added

- `/2` function
- `LET-WHEN` macro
- `WHILE` macro

## [1.0.0] 2017-02-03

Initial release

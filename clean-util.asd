;;;; This file is a part of the Clean Util library.

(in-package :asdf-user)

#-asdf3.1 (error "Clean Test requires ASDF 3.1 or later. Please upgrade your ASDF.")

(defsystem :clean-util
  :description "General purpose utilities"
  :long-description #.(read-file-string "README.md")
  :author "Philipp Matthias Schäfer <philipp.matthias.schaefer@posteo.de>"
  :licence "Public Domain / 0-clause MIT"
  :version "2.0.0"
  :class :package-inferred-system
  :depends-on (:clean-util/source/all)
  :in-order-to ((test-op (load-op :clean-util/tests/all)))
  :perform (test-op (o c) (progn
                            (load-system :clean-standard-out-test-reporter)
                            (symbol-call :clean-test :run-all))))

;;; Systems
(register-system-packages :clean-util/source/all
                          '(:clean-util/source/all))

;;; Additional Systems for Tests
(register-system-packages :clean-util/tests/all
                          '(:clean-util/tests/all))

# Clean Test Library

The Clean Util library is a collection of general purpose utilities for the
Common Lisp language.

## Clean Project

This library is part of the [Clean Project](https://gitlab.com/clean),
the outlet for my NIH syndrome.

## License

This library was written by Philipp Matthias Schäfer
(philipp.matthias.schaefer@posteo.de) and was released into the public domain,
if possible in a given jurisdiction. See [LICENSE] for details.
